import com.zuit.example.Car;
import com.zuit.example.Dog;

public class Main {
    public static void main(String[] args) {

        Car myCar = new Car();
        myCar.drive();
        System.out.println(myCar.getDriverName());
        // setters
        myCar.setBrand("Kia");
        myCar.setName("Sorrento");
        myCar.setYear_make(2022);
        System.out.println("The " + myCar.getBrand() + " " +  myCar.getName() + " " + myCar.getYear_make() + " was driven by " + myCar.getDriverName());


        myCar.setBrand("Toyota");
        System.out.println("My new car is " + myCar.getBrand());

        //another Car
        Car yourCar = new Car();
        System.out.println("Your car was driven by: " + myCar.getDriverName());

        // Animal and Dog

        Dog myPet = new Dog();
        myPet.setName("Bordagol");
        myPet.setColor("Black");
        myPet.speak();
        System.out.println(myPet.getName() + " " + myPet.getColor() + " " + myPet.getBreed());

    }
}
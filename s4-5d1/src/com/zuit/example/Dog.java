package com.zuit.example;

public class Dog extends Animal {
    // extends - keyword used to inherit the properties of a class
    private String breed;

    // constructors
    public Dog(){
        super(); // -> calls the Animal() constructor
        this.breed = "Golden Retriever";
    }

    public Dog(String name, String color, String breed){
        super(name, color); // -> calls the Animal(String name, String color) constructor
        this.breed = breed;
    }

    public String getBreed(){
        return this.breed;
    }

    public void setDogBreed(String dogBreed){
        this.breed = breed;
    }

    public void  speak(){
        super.call(); // -> calls the call() method from Animal class
        System.out.println("Woof");
    }
}

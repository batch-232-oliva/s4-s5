package com.zuit.example;

public class Car {
    // Object Oriented Concepts
    // Object - abstract idea that represents something in the real world
    // Class - representation of the object using code
    // Instance - unique copy of the idea, made "physical"

    // Objects
        // States and Attributes - what is the idea about?
        // Behaviors - what can idea do?
    // Example:  A person has attributes like name, age, height and weight. And a person can eat, sleep, and speak

    // 4 four parts of the class
    // Properties - characteristics of the objects
        // private - accessible within the class
        private String name;
        private  String brand;
        private int year_make;

        private Driver d;

    // Constructors - used to create an object
        // parameterized constructors
        public Car(String name, String brand, int year_make){
            this.name = name;
            this.brand = brand;
            this.year_make = year_make;
            this.d = new Driver("Alejandro");
        }
        // public constructor
        public Car(){
            this.d = new Driver("Alejandro"); // for every new car created, there will be a driver named Alejandro
        };

    // Getters/Setters-get and set the values of each property of the object
        // getters - retrieving the value of the instantiated object
        public String getName(){
            return this.name;
        }
        public String getBrand(){
            return this.brand;
        }
        public int getYear_make(){
            return this.year_make;
        }

        // setters-sets the value
        public void setName(String name){
            this.name = name;
        }
        public void setBrand(String brand){
            this.brand = brand;
        }
        public void setYear_make(int year_make){
            this.year_make = year_make;
        }


    // Methods-functions an object can perform
        public void drive(){
            System.out.println("The car goes vroom vroom!");
        }

        public String getDriverName(){
            return  this.d.getName();
        }
}
/* JAVA OOP CONCEPTS
Encapsulation -  mechanism of wrapping the data(variables) and code acting on the data(methods) together as a single unit
To achieve encapsulation
    --> declare the variable of a class as private
    --> provide public setter and getter methods to modify and view the variables

Composition - modelling objects that are made up of other object. it defines "has a relationship"
Inheritance - modelling objects that is a subset of another object. It defines "is a relationship"
*/
